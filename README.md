# solarizedstartpage

I have Created a Minimal Startpage based on solarized color schemes. Feel free to fork my repos. If you want to use my website here's the link: https://solarizedstartpage.000webhostapp.com/



##Cloning The Repo
```

- Use this command to clone my repo
git clone https://gitlab.com/Xeniel/solarizedstartpage.git

```

##Dependencies
- Cybeway Riders.ttf needs to be installed.Added in the Repositories.

##Known Issues
- Enabling Dark Reader extension may break the site.Try to turn dark reader off Only for this site untill I find a fix for this issue.

##Previews

 
1. [Preview1](https://gitlab.com/Xeniel/solarizedstartpage/-/blob/main/Preview1.PNG)
2. [Preview2](https://gitlab.com/Xeniel/solarizedstartpage/-/blob/main/Preview2.PNG)


